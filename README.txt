NodeStream Feed Importer module

This module create content feeds for NodeStream based on Google Spreadsheets and it is used like this:

1 Install NodeStream (drupal.org/project/nodestream)
1 Download code from git and place it in you sites/all/modules folder (or where you want to store it)

2 Also download (dependencies) feeds, feeds_tamper, media_feeds and job_scheduler (needed by feeds)
 
3 Install this feature by enabling it in /admin/structure/features

4 Go to /import/ns_feed_importer_contributor_import 

5 Enter the following url to fetch some demo contributor content into your site
https://docs.google.com/spreadsheet/pub?key=0Au7RA7y0kUnrdGlYd1dYZXE0MFlSNWQxS3kyVkdmcHc&single=true&gid=0&output=csv

6 Go to /import/ns_feed_importer_article_import

7 Enter the following url to import article demo content

https://docs.google.com/spreadsheet/pub?key=0Au7RA7y0kUnrdGxFajZCak5oMjAwX0RBdjJlbFBMekE&single=true&gid=0&output=csv


If you want to create your own content you can either store this file locally and update it by changinh the importer settings
admin/structure/feeds/edit/ns_feed_importer_article_import/fetcher
to import from file


Known Issues:

If you get errors like:
cURL error (60) SSL certificate problem
you can do a quick fix in the feeds module by adding the following line Around row 160

curl_setopt($download, CURLOPT_SSL_VERIFYPEER, 0); 

to the file <feeds>/libraries/http_request.inc



Features to come:
We are going to extend this feature to also include reference fields, wich is not supported right now.




