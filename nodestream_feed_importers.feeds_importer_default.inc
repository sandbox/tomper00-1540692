<?php
/**
 * @file
 * nodestream_feed_importers.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function nodestream_feed_importers_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'ns_feed_importer_article_import';
  $feeds_importer->config = array(
    'name' => 'NodeStream Article',
    'description' => 'Import Articles to NodeStream',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => FALSE,
        'use_pubsubhubbub' => FALSE,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'ns_article',
        'expire' => '-1',
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Kicker',
            'target' => 'field_ns_article_kicker',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Lead',
            'target' => 'field_ns_article_lead',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Body',
            'target' => 'field_ns_article_body',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Media',
            'target' => 'field_ns_article_media',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Region',
            'target' => 'field_ns_prod_news_region',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Topic',
            'target' => 'field_ns_prod_news_topic',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'RSS Topic',
            'target' => 'field_ns_prod_rss_topic',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '1',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['ns_feed_importer_article_import'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'ns_feed_importer_contributor_import';
  $feeds_importer->config = array(
    'name' => 'NodeStream Contributors',
    'description' => 'Import Contributors to NodeStream',
    'fetcher' => array(
      'plugin_key' => 'FeedsHTTPFetcher',
      'config' => array(
        'auto_detect_feeds' => 0,
        'use_pubsubhubbub' => 0,
        'designated_hub' => '',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'content_type' => 'ns_contributor',
        'expire' => '-1',
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'About',
            'target' => 'field_ns_contributor_about',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Photo',
            'target' => 'field_ns_contributor_photo',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Email',
            'target' => 'field_ns_contributor_email',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '1',
        'input_format' => 'html_editor',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '1800',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['ns_feed_importer_contributor_import'] = $feeds_importer;

  return $export;
}
