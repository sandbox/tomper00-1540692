<?php
/**
 * @file
 * nodestream_feed_importers.feeds_tamper_default.inc
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function nodestream_feed_importers_feeds_tamper_default() {
  $export = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'ns_feed_importer_article_import-topic-explode';
  $feeds_tamper->importer = 'ns_feed_importer_article_import';
  $feeds_tamper->source = 'Topic';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => '|',
    'limit' => '',
    'real_separator' => '|',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode';
  $export['ns_feed_importer_article_import-topic-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'ns_feed_importer_contributor_import-title-html_entity_encode';
  $feeds_tamper->importer = 'ns_feed_importer_contributor_import';
  $feeds_tamper->source = 'Title';
  $feeds_tamper->plugin_id = 'html_entity_encode';
  $feeds_tamper->settings = '';
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'HTML entity encode';
  $export['ns_feed_importer_contributor_import-title-html_entity_encode'] = $feeds_tamper;

  return $export;
}
